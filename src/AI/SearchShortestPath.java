package AI;

import java.util.ArrayList;

import javafx.scene.control.TextArea;

public class SearchShortestPath implements ISearch{

	private Graph graph;
	private TextArea textArea;

	public SearchShortestPath(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		if (!graph.containsNode(start) || !graph.containsNode(end)) {
			return null;
		}
		graph.resetAll();

		Node temp = graph.getNode(start);
		temp.km = 0;
		ArrayList<Links> queue = new ArrayList<>();
		queue.addAll(temp.listLinks);
		while (!queue.isEmpty()) {
			System.out.println(temp.name);
			System.out.print("[");
			for (int i = 0; i < queue.size(); i++) {
				System.out.print(queue.get(i).reNode.name + ", ");
			}
			System.out.print("]\n");
			GraphUtils.printNodeInfo(temp, textArea);
			for (Links link : queue) {
				GraphUtils.setParentCostAndPrice(temp, link);
			}

			temp.tested = true;
			queue.clear();
			
			Node minNode = GraphUtils.findLowestKMInGraph(graph);
			if (minNode != null) {
				for (Links link : minNode.listLinks) {
					queue.add(link);
				}
				temp = minNode;
			}
		}

		if (graph.getNode(end).parent == null) {
			return null;
		} else {
			GraphUtils.printKMandPrice(graph, end, textArea);
			ArrayList<String> path = new ArrayList<>();
			GraphUtils.printPath(graph.getNode(start), graph.getNode(end), path);
			return path;
		}
	}
}
