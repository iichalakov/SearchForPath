package AI;

import java.util.ArrayList;

import javax.management.relation.Relation;

import javafx.scene.control.TextArea;
import AI.Links;
import AI.Graph;
import AI.Node;

public class GraphUtils {

	public static ArrayList<String> printPathByParent(Graph graph, String end) {

		ArrayList<String> path = new ArrayList<>();
		path.add(end);
		Node next = graph.getNode(end).parent;
		while (next != null) {
			System.out.println("next: " + next.name);
			path.add(next.name);
			next = next.parent;
		}
		return path;
	}

	public static void printPath(Node stopNode, Node goalNode, ArrayList<String> path) {
		if (!(goalNode.name == stopNode.name)) {
			path.add(goalNode.name);
			printPath(stopNode, goalNode.parent, path);
		} else {
			path.add(stopNode.name);
			return;
		}
	}

	public static void printShortestPath(Node stopNode, Node goalNode) {
		if (!(goalNode.name == stopNode.name)) {
			System.out.print(goalNode.name + ", ");
			printShortestPath(stopNode, goalNode.parent);
		} else {
			System.out.print(stopNode.name + "\n");
			return;
		}
	}

	public static ArrayList<Node> sortQueueByDistance(ArrayList<Node> queue, Node node) {

		boolean isNotAdded = true;

		for (int i = 0; i < queue.size(); i++) {
			if (node.distance < queue.get(i).distance) {
				queue.add(i, node);
				isNotAdded = false;
				break;
			}
		} // loop over queue
		if (isNotAdded) {
			queue.add(node);
		}

		return queue;
	}

	public static ArrayList<Node> sortQueueByDistance(ArrayList<Node> queue) {
		ArrayList<Node> a = new ArrayList<>();
		double min;
		min = queue.get(0).price;
		Node minNode = queue.get(0);
		while (!queue.isEmpty()) {
			for (Node node : queue) {
				min = queue.get(0).price;
				minNode = queue.get(0);
				if (min > node.price) {
					min = node.price;
					minNode = node;
				}
			}
			queue.remove(minNode);
			a.add(minNode);
		}
		return a;
	}

	public static Node findLowestKMInGraph(Graph graph) {
		ArrayList<Node> allNoNTestedNodes = new ArrayList<>();
		Node minNode = null;
		for (Node node : graph.getMap().values()) {
			if (!node.tested) {
				allNoNTestedNodes.add(node);
			}
		}
		if (!allNoNTestedNodes.isEmpty()) {
			double min = allNoNTestedNodes.get(0).km;
			minNode = allNoNTestedNodes.get(0);
			for (int i = 1; i < allNoNTestedNodes.size(); i++) {
				if (min > allNoNTestedNodes.get(i).km) {
					min = allNoNTestedNodes.get(i).km;
					minNode = allNoNTestedNodes.get(i);
				}
			}
		}
		if(minNode != null){
			return minNode;
		}else{
			return null;
		}
	}
	public static Node findLowestPriceInGraph(Graph graph) {
		ArrayList<Node> allNoNTestedNodes = new ArrayList<>();
		Node minPriceNode = null;
		for (Node node : graph.getMap().values()) {
			if (!node.tested) {
				allNoNTestedNodes.add(node);
			}
		}
		if (!allNoNTestedNodes.isEmpty()) {
			double min = allNoNTestedNodes.get(0).price;
			minPriceNode = allNoNTestedNodes.get(0);
			for (int i = 1; i < allNoNTestedNodes.size(); i++) {
				if (min > allNoNTestedNodes.get(i).price) {
					min = allNoNTestedNodes.get(i).price;
					minPriceNode = allNoNTestedNodes.get(i);
				}
			}
		}
		if(minPriceNode != null){
			return minPriceNode;
		}else{
			return null;
		}
		}

	public static void calcDistance(Node start, Node end) {
		start.distance = Math.sqrt(Math.pow(start.x - end.x, 2) + Math.pow(start.y - end.y, 2));
	}

	public static void printNodeInfo(Node n, TextArea textArea) {
		String parentName;
		if (n.parent == null) {
			parentName = "No parent";
		} else {
			parentName = n.parent.name;
		}
		textArea.setText(textArea.getText() + "\n" + "Node name: " + n.name + ", KM: " + n.km + ", Price: " + n.price
				+ ", Parent: " + parentName);
	}

	public static void setParentCostAndPrice(Node p, Links l) {
		double newKM = p.km + l.pLenght + p.weight;
		System.out.println("newKM: " + newKM);
		double newPrice = p.price + l.price;
		Node rNode = l.reNode;

		if (rNode.km > newKM) {
			rNode.km = newKM;
			rNode.parent = p;
			rNode.price = newPrice;
		}
	}

	public static void setParentByPrice(Node p, Links l) {
		if(p.km == Double.MAX_VALUE){
			p.km = 0;
		}
		double newKM = p.km + l.pLenght + p.weight;
		double newPrice = p.price + l.price;
		Node rNode = l.reNode;
		if (rNode.parent == null || rNode.price > newPrice) {
			rNode.km = newKM;
			rNode.parent = p;
			rNode.price = newPrice;
		}
	}

	public static double calculatePrice(Links l) {
		double newPrice = 0;
		switch (l.rType) {
		case 0:
			newPrice += calculateType0Price(l.pLenght);
			break;
		case 1:
			newPrice += calculateType1Price(l.pLenght);
			break;
		default:
			break;
		}
		return newPrice;
	}

	public static double calculateType0Price(double length) {
		double price = 3;
		length -= 10;
		if (length > 0) {
			price += length / 2;
		}
		return price;
	}

	public static double calculateType1Price(double lenght) {
		double price = 0;
		int a;
		lenght -= 5;
		if (lenght > 0) {
			a = (int) (lenght / 10);
			price = (a + 1) * 2;
		}
		return price;
	}

	public static void setParentCost(Node p, Links l) {
		double newCost = p.km + l.pLenght;
		Node rNode = l.reNode;
		if (rNode.parent == null || rNode.km > newCost) {
			rNode.km = newCost;
			rNode.parent = p;
		}
	}

	public static void printKMandPrice(Graph graph, String end, TextArea textArea) {
		textArea.setText(textArea.getText() + "\n" + "KM: " + graph.getNode(end).km);
		textArea.setText(textArea.getText() + "\n" + "Price: " + graph.getNode(end).price);
	}

}
