package AI;

import java.util.ArrayList;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Node implements INode {

	public String name;
	public double x, y;
	public double weight;
	public double distance;
	public double km;
	public double price;
	//public ArrayList<Node> links = new ArrayList<Node>();
	//Node , Lenght
	public ArrayList<Links> listLinks = new ArrayList<>();
	
	// flags
	public boolean tested, expanded;
	public int depth;
	public Node parent;

	public Node(String name) {
		this.name = name;
		parent = null;
	}

	public Node(String name, double weight) {
		this.name = name;
		this.weight = weight;
	}
	
	public Node(String name, double weight, double x, double y) {
		this.name = name;
		this.weight = weight;

	}

	public void reset() {
		this.tested = false;
		this.expanded = false;
		this.depth = 0;
		this.parent = null;
		//this.km = Double.MAX_VALUE;
		this.price = 0;
	}


	public void removeLink(String toLink) {
		for(Links link : listLinks){
			if(link.reNode.name.equals(toLink)){
				listLinks.remove(link);
				break;
			}
		}
	}

}// end Node
