package AI;

import java.util.ArrayList;

import javafx.scene.control.TextArea;

public class GreedySearchByCoordinates implements ISearch {

	Graph graph;
	private TextArea textArea;

	public GreedySearchByCoordinates(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		if (!graph.containsNode(start) || !graph.containsNode(start)) {
			return null;
		}

		graph.resetAll();

		ArrayList<Node> queue = new ArrayList<>();
		Node temp;
		queue.add(graph.getNode(start));
		
		while (!queue.isEmpty()) {
			temp = queue.get(0);
			GraphUtils.printNodeInfo(temp, textArea);
			queue.remove(0);
			temp.tested = true;
			if (temp.name.equals(end)) {
				GraphUtils.printKMandPrice(graph, end, textArea);
				ArrayList<String> path = new ArrayList<>();
				GraphUtils.printPath(graph.getNode(start), graph.getNode(end), path);
				return path;
			}
			if (!temp.expanded) {
				for (Links link : temp.listLinks) {
					Node rNode = link.reNode;
					if (!link.reNode.tested && !queue.contains(rNode)) {
						GraphUtils.calcDistance(rNode, graph.getNode(end));
						GraphUtils.setParentByPrice(temp, link);
						GraphUtils.sortQueueByDistance(queue, rNode);
						rNode.parent = temp;
					}
				}
				temp.expanded = true;
			}

		} // end while
		return null;
	}// end hasPath

}
