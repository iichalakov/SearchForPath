package AI;

import java.util.ArrayList;

import javafx.scene.control.TextArea;

public class breathSearch implements ISearch{
	private Graph graph;
	private TextArea textArea;

	public breathSearch(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		graph.resetAll();
		if (!graph.containsNode(start) || !graph.containsNode(end)) {
			return null;
		}

		ArrayList<Node> queue = new ArrayList<>();
		Node temp;
		queue.add(graph.getNode(start));

		while (!queue.isEmpty()) {
			temp = queue.get(0);
			GraphUtils.printNodeInfo(temp, textArea);
			queue.remove(0);
			temp.tested = true;
			if (temp.name.equals(end)) {
				GraphUtils.printKMandPrice(graph, end, textArea);
				ArrayList<String> path = new ArrayList<>();
				GraphUtils.printPath(graph.getNode(start), graph.getNode(end), path);
				return path;
			}
			if (!temp.expanded) {
				for (Links link : temp.listLinks) {
					Node rNode = link.reNode;
					if (!link.reNode.tested) {
						GraphUtils.setParentByPrice(temp, link);
						rNode.tested = true;
						rNode.parent = temp;
						queue.add(rNode);
						//queue.add(0, node);
					}
				}
				temp.expanded = true;
			}
		} // end while
		
		return null;

	}// end hasPath

	public void printQueue(ArrayList<Node> arr) {
		for (Node node : arr) {
			System.out.print(node.name + ", ");
		}
	}
}
