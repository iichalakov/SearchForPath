package AI;

import java.util.ArrayList;

import javafx.scene.control.TextArea;

public class SeachCheapestPath implements ISearch{


	private Graph graph;
	private TextArea textArea;

	public SeachCheapestPath(Graph graph, TextArea textArea) {
		this.graph = graph;
		this.textArea = textArea;
	}

	@Override
	public ArrayList<String> hasPath(String start, String end) {
		if (!graph.containsNode(start) || !graph.containsNode(start)) {
			return null;
		}

		graph.resetAll();

		ArrayList<Node> queue = new ArrayList<>();
		Node temp;
		queue.add(graph.getNode(start));
		while (!queue.isEmpty()) {
			temp = queue.get(0);
			GraphUtils.printNodeInfo(temp, textArea);
			queue.remove(0);
			temp.tested = true;
			if (!temp.expanded) {
				for (Links link : temp.listLinks) {
					Node rNode = link.reNode;
					GraphUtils.setParentByPrice(temp, link);
					queue.add(rNode);
					queue = GraphUtils.sortQueueByDistance(queue);
				}
				temp.expanded = true;
			}
		} 

		if (graph.getNode(end).parent == null) {
			return null;
		} else {
			GraphUtils.printKMandPrice(graph, end, textArea);
			ArrayList<String> path = new ArrayList<>();
			GraphUtils.printPath(graph.getNode(start), graph.getNode(end), path);
			return path;
		}
	}

}
