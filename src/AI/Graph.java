package AI;

import java.util.HashMap;

public class Graph {
	
	private HashMap<String, Node> map = new HashMap<String, Node>();
	
	public void addNode(Node node) {
		// TODO Auto-generated method stub
		map.put(node.name, node);
	}

	public void oneWayLink(String start, String end, double length, int roadType){
		if(map.containsKey(start) && map.containsKey(end)){
			//map.get(start).links.add(map.get(end));
			Links link = new Links(map.get(end), length, roadType);
			link.setPrice(GraphUtils.calculatePrice(link));
			map.get(start).listLinks.add(link);
		}else{
			System.out.println("Missing nodes");
		}
	}
	
	public void twoWayLink(String start, String end, double length, int roadType){
		oneWayLink(start, end, length, roadType);
		oneWayLink(end, start, length, roadType);
	}

	public void resetAll() {
		for (Node node : map.values()) {
			node.reset();
		}
	}
	
	public void clearAll(){
		map.clear();
	}
	
	public boolean containsNode(String name){
		return map.containsKey(name);
	}
	
	public Node getNode(String name){
		return map.get(name);
	}	
	
	public HashMap<String, Node> getMap() {
		return map;
	}
	
	public void deleteNode(Node node) {
		for(Node nodes : map.values()){
			for(Links nodesLinks: nodes.listLinks){
				map.get(nodesLinks.reNode.name).removeLink(node.name);
			}
		}
		map.remove(node.name);
	}
	
	public void printAllLinks(){
		for(Node node: map.values()){
			System.out.println("Node " + node.name + " has: ");
			for(Links links : node.listLinks){
				System.out.println("Link: " + links.reNode.name);
			}
		}
	}

}
