package UI;

import AI.Graph;
import AI.Node;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;

public class AddingNodes extends Dialog<Node> {
	private Node node;

	public AddingNodes(Graph graph) {
		super();

		Label cityName = new Label();
		cityName.setText("City name: ");
		Label cityWeight = new Label();
		cityWeight.setText("City weight: ");

		TextField cityNameTextField = new TextField();
		TextField cityWeightTextField = new TextField();

		GridPane grid = new GridPane();
		grid.add(cityName, 1, 1);
		grid.add(cityNameTextField, 2, 1);
		grid.add(cityWeight, 1, 2);
		grid.add(cityWeightTextField, 2, 2);

		DialogPane dialog = getDialogPane();
		setTitle("Add a city");
		dialog.setContent(grid);

		ButtonType buttonTypeAdd = new ButtonType("Add", ButtonData.OK_DONE);
		ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		dialog.getButtonTypes().addAll(buttonTypeAdd, buttonTypeCancel);

		setResultConverter(dialogButton -> {
			return node;
		});

		final Button okButton = (Button) dialog.lookupButton(buttonTypeAdd);
		okButton.addEventFilter(ActionEvent.ACTION, event -> {
			if (isFloat(cityWeightTextField) && !isEmpty(cityWeightTextField) && !isNameTaken(cityNameTextField, graph)) {
				node = new Node(cityNameTextField.getText().toUpperCase(), Float.parseFloat(cityWeightTextField.getText()));
			} else {
				event.consume();
			}
		});

		cityWeightTextField.setOnKeyPressed(e -> {
			cityWeightTextField.setStyle("-fx-text-fill: rgb(0, 0, 0);");
		});
		
		cityNameTextField.setOnKeyPressed(e -> {
			cityNameTextField.setStyle("-fx-text-fill: rgb(0, 0, 0);");
		});
	}

	private boolean isFloat(TextField textfield) {
		try {
			Float.parseFloat(textfield.getText());
			return true;
		} catch (NumberFormatException e) {
			textfield.setStyle("-fx-text-fill: rgb(255, 0, 0);");
			return false;
		}
	}

	private boolean isEmpty(TextField textfield) {
		if (textfield.getText().equals("")) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isNameTaken(TextField textfield, Graph graph){
		for(Node node: graph.getMap().values()){
			if(node.name.equals(textfield.getText())){
				textfield.setStyle("-fx-text-fill: rgb(255, 0, 0);");
				return true;
			}
		}
		return false;
	}
}
