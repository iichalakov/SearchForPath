package UI;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import AI.Graph;
import AI.Links;
import AI.Node;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class SaveData {
	
	public static void save(Graph graph, Stage stage){
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File(System.getProperty("user.dir") + "\\save"));
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
		fileChooser.getExtensionFilters().add(extFilter);
		fileChooser.setTitle("Save Graph");
		File file = fileChooser.showSaveDialog(stage);
		

		PrintWriter printWriter = null;
		BufferedWriter bufferedWriter = null;
		if (file != null) {
			try {
				printWriter = new PrintWriter(file);
				bufferedWriter = new BufferedWriter(new PrintWriter(printWriter));
				for (Node node : graph.getMap().values()) {
					bufferedWriter.write("#" + node.name + "," + node.weight + "," + node.x + "," + node.y + "#");
					bufferedWriter.newLine();
				}
				for (Node node : graph.getMap().values()) {
					for (Links link : node.listLinks) {
						bufferedWriter.write("$" + node.name + "," + link.reNode.name + "," + link.pLenght + "," + link.rType + "$");
						bufferedWriter.newLine();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				printWriter.close();
			} finally {
				try {

					if (bufferedWriter != null)
						bufferedWriter.close();

					if (printWriter != null)
						printWriter.close();

				} catch (IOException ex) {

					ex.printStackTrace();

				}
			}
		}
	
	}
}
