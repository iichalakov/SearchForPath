package UI;



import java.util.ArrayList;

import AI.Graph;
import AI.GreedySearchByCoordinates;
import AI.ISearch;
import AI.SeachCheapestPath;
import AI.SearchShortestPath;
import AI.breathSearch;
import AI.Node;
import AI.Links;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class UI extends Application {
		
		Graph graph;
	
		ObservableList<String> options = FXCollections.observableArrayList("Breath First","GreedyByCoordinates","Shortest Path", "Cheapest Path");
		static ArrayList<String> pathAR = new ArrayList<>();
		ArrayList<String> pointsToGoFirst = new ArrayList<>();
		ComboBox<String> comboBoxPoints;
		ComboBox<String> searchAlgorithems;
		
		static Boolean noPath = false;
		
		ToolBar tb;
		ToolBar secondbar;
		
		Button save;
		Button open;
		Button searchB;
		Button addPoint;
		Button reset;
		
		Label startLabel;
		Label endLabel;
		Label goThroughFirst;
		
		TextField startTF;
		TextField endTF;
		
		TextArea textArea ;
		
		MenuItem addNodes;
		MenuItem addLink;
		MenuItem add;
		MenuItem edit;
		MenuItem delete;

		
		final ContextMenu CONTEXTMENUNODELINK = new ContextMenu();
		//final ContextMenu CONTEXTMENULINK = new ContextMenu();
		
		final ContextMenu contextMenuAddEditDelete = new ContextMenu();
		final ContextMenu contextMenuDelete = new ContextMenu();
		
		public UI(Stage stage) {
			try {
				init();
				start(stage);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public static void newStage(Stage stage){
			new UI(stage);
		}
		
		@Override
		public void start(Stage stage) throws Exception {
			
			graph = new Graph();
			
			BorderPane borderpane = new BorderPane();
			
			//borderpane.setCenter(window);
			
			tb = new ToolBar();
			secondbar = new ToolBar();
			save = new Button("Save");
			open = new Button("Open");
			searchB = new Button("Search");
			addPoint = new Button("Point to Add");
			reset = new Button("Reset");
			
			startLabel = new Label("From :");
			startTF = new TextField();
			endLabel = new Label("To :");
			endTF = new TextField();
			goThroughFirst = new Label();
			
			textArea = new TextArea();
			textArea.setMaxWidth(600);
			textArea.setMaxHeight(400);
			textArea.setEditable(false);
			
			comboBoxPoints = new ComboBox<>();
			searchAlgorithems = new ComboBox<>(options);
			
			if (!searchAlgorithems.getItems().isEmpty()) {
				searchAlgorithems.setValue(searchAlgorithems.getItems().get(0));
			}
			
			addNodes = new MenuItem("Add new Node");
			addLink = new MenuItem("Add Link");
	
			/*contextMenuAddEditDelete.getItems().addAll(add, edit, delete);
			contextMenuDelete.getItems().add(delete);*/
			
			CONTEXTMENUNODELINK.getItems().addAll(addNodes,addLink);
		
			
			tb.getItems().addAll(save, open, new Separator(),startLabel,startTF,endLabel,endTF,new Separator(),searchB,searchAlgorithems,new Separator(),comboBoxPoints, addPoint,goThroughFirst,new Separator(),reset);
			//secondbar.getItems().addAll(comboBoxPoints, addPoint,goThroughFirst);
			
			borderpane.setTop(new VBox(tb,secondbar));
			borderpane.setCenter(textArea); 

			Scene scene = new Scene(borderpane, 1000, 600);
			
			scene.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent e) {
					if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 1) {
						CONTEXTMENUNODELINK.show(stage, e.getScreenX(), e.getScreenY());
					}
				}
			});
			
			addPoint.setOnAction(e -> {
				pointsToGoFirst.add(comboBoxPoints.getSelectionModel().getSelectedItem());
				goThroughFirst
						.setText(goThroughFirst.getText() + comboBoxPoints.getSelectionModel().getSelectedItem() + ", ");
			});
			save.setOnAction(e -> {
				SaveData.save(graph, stage);				
			});
			
			open.setOnAction(e -> {
				OpenData.open(graph, stage);	
				fillComboBoxWithGraphPoints(graph, comboBoxPoints);
			});
			
			
			addLink.setOnAction(e -> {
				new AddLinks(graph).showAndWait().ifPresent((Boolean g) -> {
				});
			});
			
			addNodes.setOnAction(e -> {
				new AddingNodes(graph).showAndWait().ifPresent((Node node) -> {
					if (node != null) {
						graph.addNode(node);
						comboBoxPoints.getItems().add(node.name);
					}
				});
				
			});
			reset.setOnAction(e -> {
				graph.clearAll();
			});

			
			addLink.setOnAction(e_-> {
				new AddLinks(graph).showAndWait().ifPresent((Boolean g) -> {
				});
			});
			
			
			searchB.setOnAction(e -> {
				textArea.clear();
				switch (searchAlgorithems.getSelectionModel().getSelectedItem()) {
				case "Breath First":
					pointsToGoFirst.add(0, startTF.getText().toUpperCase());
					pointsToGoFirst.add(endTF.getText().toUpperCase());
					setTextToTextArea("Breath First", textArea);
					for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
						if(noPath){
							break;
						}
						search(new breathSearch(graph, textArea), pointsToGoFirst.get(i), pointsToGoFirst.get(i + 1), graph,
								 textArea);
					}
					printPathToTextArea(textArea);
					resetSearch();
					break;
			
				case "Shortest Path":
					pointsToGoFirst.add(0, startTF.getText().toUpperCase());
					pointsToGoFirst.add(endTF.getText().toUpperCase());
					setTextToTextArea("Shortest Path", textArea);
					for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
						if(noPath){
							break;
						}
						search(new SearchShortestPath(graph, textArea), pointsToGoFirst.get(i), pointsToGoFirst.get(i + 1),
								graph, textArea);
					}
					printPathToTextArea(textArea);
					resetSearch();
					break;

				case "Cheapest Path":
					pointsToGoFirst.add(0, startTF.getText().toUpperCase());
					pointsToGoFirst.add(endTF.getText().toUpperCase());
					setTextToTextArea("Cheapest Path", textArea);
					for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
						if(noPath){
							break;
						}
						search(new SeachCheapestPath(graph, textArea), pointsToGoFirst.get(i), pointsToGoFirst.get(i + 1),
								graph,textArea);
					}
					printPathToTextArea(textArea);
				
					resetSearch();
					break;
					
				case "GreedyByCoordinates":
					pointsToGoFirst.add(0, startTF.getText().toUpperCase());
					pointsToGoFirst.add(endTF.getText().toUpperCase());
					setTextToTextArea("Greedy By Coordinates", textArea);
					for (int i = 0; i < pointsToGoFirst.size() - 1; i++) {
						if(noPath){
							break;
						}
						search(new GreedySearchByCoordinates(graph, textArea), pointsToGoFirst.get(i),
								pointsToGoFirst.get(i + 1), graph,textArea);
					}
					printPathToTextArea(textArea);
					resetSearch();
					break;
				}

			});
			
			stage.setTitle("Seach for Path");
			stage.setScene(scene);
			stage.show();
						
			init(graph);
			fillComboBoxWithGraphPoints(graph, comboBoxPoints);
		}//end start stage
		
		
		public static void search(ISearch s, String start, String end, Graph graph, TextArea textArea) {
			ArrayList<String> tempAR = new ArrayList<>();
			tempAR = s.hasPath(start, end);
			textArea.setText(textArea.getText() + "\n");
			if (tempAR == null) {
				textArea.setText(textArea.getText() + "\n" + "No path found!");
				noPath = true;
			} else {
				for (int i = tempAR.size() - 1; i >= 0; i--) {
					textArea.setText(textArea.getText() + tempAR.get(i) + " -> ");
					pathAR.add(tempAR.get(i));
				}
			}
		}
		private void setTextToTextArea(String string, TextArea textArea) {
			if (textArea.getText().equals("")) {
				textArea.setText(
						textArea.getText() + "New search started: " + string + "\nChosen cities: " + pointsToGoFirst);
			} else {
				textArea.setText(textArea.getText() + "\n\n" + "New search started: " + string + "\nChosen cities: "
						+ pointsToGoFirst);
			}
		}
		private void resetSearch(){
			goThroughFirst.setText("");
			pointsToGoFirst.clear();
			pathAR.clear();
			noPath = false;			
		}
		private void printPathToTextArea(TextArea textArea) {
			
			textArea.setText(textArea.getText() + "\n" + "Full path is: ");
			for (int i = 0; i < pathAR.size(); i++) {
				textArea.setText(textArea.getText() + pathAR.get(i) + " -> ");
			}
		}

		public static void init(Graph g) {
			Node A = new Node("A");
			Node B = new Node("B");
			Node C = new Node("C");
			Node D = new Node("D");
			Node E = new Node("E");
			Node F = new Node("F");

			g.addNode(A);
			g.addNode(B);
			g.addNode(C);
			g.addNode(D);
			g.addNode(E);
			g.addNode(F);

			g.twoWayLink("A", "B", 20, 0);
			g.twoWayLink("A", "C", 5, 1);
			g.twoWayLink("B", "D", 40, 2);
			g.twoWayLink("B", "F", 50, 0);
			g.twoWayLink("B", "E", 50, 1);
			g.twoWayLink("C", "F", 60, 0);
			g.twoWayLink("E", "F", 10, 1);
		}
		
		private void fillComboBoxWithGraphPoints(Graph graph, ComboBox<String> cb) {
			cb.getItems().clear();
			for (Node node : graph.getMap().values()) {
				cb.getItems().add(node.name);
			}
			cb.getSelectionModel().select(0);
		} 
		
}//END CLASS





