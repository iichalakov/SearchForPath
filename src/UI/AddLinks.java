package UI;

import AI.Graph;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AddLinks extends Dialog<Boolean> {
		
	TextField TFAddFrom;
	TextField TFAddTo;
	TextField TFAddWays;
	TextField TFAddType;
	TextField TFRemoveFrom;
	TextField TFRemoveTo;
	TextField TFRemoveWays;
	TextField TFKilometer;
	int checkWay;
	
	@SuppressWarnings("finally")
	public AddLinks(Graph graph){
		HBox hbox = new HBox();
		TFAddFrom = new TextField();
		TFAddFrom.setMinWidth(50);
		TFAddTo = new TextField();
		TFAddTo.setMinWidth(50);
		TFAddWays = new TextField();
		TFAddWays.setMinWidth(50);
		TFAddType = new TextField();
		TFAddType.setMinWidth(50);
		
		TFKilometer = new TextField();
		TFKilometer.setMinWidth(50);
		Button add = new Button("Add");
		VBox fromBox = new VBox(new Label("From"), TFAddFrom, new Separator());
		VBox toBox = new VBox(new Label("To"), TFAddTo, new Separator());
		VBox waysBox = new VBox(new Label("Ways"), TFAddWays, new Separator());
		VBox typeBox = new VBox(new Label("Type"), TFAddType, new Separator());
		VBox kilometerBox = new VBox(new Label("Kilometers"), TFKilometer);
		VBox buttonsBox = new VBox(new Label("Buttons"), add, new Separator());
		hbox.getChildren().addAll(fromBox, toBox, waysBox, typeBox, kilometerBox, buttonsBox);

		BorderPane pane = new BorderPane();
		pane.setCenter(hbox);

		add.setOnAction(e -> {
			String fromLink = TFAddFrom.getText().toUpperCase();
			String toLink = TFAddTo.getText().toUpperCase();
			String ways = TFAddWays.getText().toUpperCase();
			int type = 0;
			double km = 0;
			try {
				type = Integer.parseInt(TFAddType.getText());
				km = Double.parseDouble(TFKilometer.getText());
			} catch (NumberFormatException e2) {
				System.out.println(e2);
				resetTextBoxes();
				//make an error
			}
			if (type >= 0 && type <= 2) {
				if (doesGraphContainsNodes(graph, fromLink, toLink)) {
					if (ways.equals("1")) {
						graph.getNode(fromLink).removeLink(toLink);
						graph.oneWayLink(fromLink, toLink, km, type);
					} else if (ways.equals("2")) {
						graph.getNode(fromLink).removeLink(toLink);
						graph.getNode(toLink).removeLink(fromLink);
						graph.twoWayLink(fromLink, toLink, km, type);
					}
				}
			}else{
				//make error
			}
			resetTextBoxes();
		});

		DialogPane dialog = getDialogPane();
		setTitle("Add or remove links from cities");
		dialog.setContent(pane);

		ButtonType buttonTypeApply = new ButtonType("Done", ButtonData.OK_DONE);
		dialog.getButtonTypes().addAll(buttonTypeApply);

		setResultConverter(dialogButton -> {
			try {
				final ButtonData data = dialogButton.getButtonData();
				if (data == ButtonData.OK_DONE) {
					return true;
				} else {
					return false;
				}
			} catch (NullPointerException e) {
				System.out.println("Exit with top right X");
			} finally {
				return false;
			}
		});
	}
	public boolean doesGraphContainsNodes(Graph graph, String from, String to) {
		if (graph.containsNode(from) && graph.containsNode(to)) {
			return true;
		} else {
			return false;
		}
	}

	public void resetTextBoxes() {
		TFAddFrom.clear();
		TFAddTo.clear();
		TFAddWays.clear();
		TFAddType.clear();
		TFKilometer.clear();
	}
	
}	
